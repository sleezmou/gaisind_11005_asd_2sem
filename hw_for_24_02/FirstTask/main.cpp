#include <iostream>

using namespace std;

int sum(int *a, int size) {
    int result = 0;

    for(int * p = a; p < a + size; p++) {
        result += *p;
    }

    return result;
}

int main() {
    int size = 6;
    int array[6] = {1, 2, 3, 4, 5, 7};

    cout << sum(array, size);

    return 0;
}
