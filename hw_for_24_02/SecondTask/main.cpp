#include <iostream>

using namespace std;

void connectArrays(int * x, int * y, int size1, int size2) {
    int * z = new int[size1 + size2];
    for(int * i = z; i < z + size1; i++) {
        * i = * x;
        x++;
    }
    for(int * i = z + size1; i + size1 < z + size2; i++) {
        * i = * y;
        y++;
    }
    for(int * i = z; i < z + size1 + size2; i++) {
        cout << * i << " ";
    }
}
int main() {
    int size1 = 5;
    int size2 = 4;
    int * x = new int[size1] {1, 2, 3, 4, 5};
    int * y = new int[size2] {12, 13, 14, 15};

    connectArrays(x, y, size1, size2);
    delete[] x;
    delete[] y;

    return 0;
}
