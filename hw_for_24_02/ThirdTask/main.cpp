#include <iostream>

using namespace std;

void sort(int * a, int size) {
    for (int *i = a; i < a + size - 1; i++)
        for (int *j = i + 1; j < a + size; j++)
            if (*i > *j) {
                int x = *i;
                *i = *j;
                *j = x;
            }
}

int main() {
    int size = 7;
    int a[] = {5, 9, 4, 16, 1, 7, 8};

    sort(a, size);

    for (int *p = a; p < a + size; p++)
        cout << *p << " ";

    return 0;
}